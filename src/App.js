import React, { Component } from 'react';
import './App.css';
import Login from './components/Login';
import DeptTable from './components/DeptTable';
import StudentTable from './components/StudentTable';
import FormField from './components/FormField';



class App extends Component {

  

  render() {

    return (
      <div>
      <div className="App layout">
        <h1>CHRIS COLLEGE STUDENT DIRECTORY</h1>
        <Login />
      </div>
        <DeptTable />
        {/* <StudentTable StudentList={this.state.StudentList} onDelete={this.onDelete.bind(this)} /> */}
        <StudentTable />
        <FormField />
        {/* <div className="layout">
          <DeptList departments={this.state.departments} deptClicked={this.deptClicked}/>
          <DeptDetails dept={this.state.selected}/>
         
        </div> */}
      </div>
    );
  }
  
}

export default App;
