import React from 'react';


const Login = () => {
    return (
        
          <div className="ui middle aligned center aligned grid">
            <div className="column">
              <h3 class="image header">
                <div class="content">Admin user login</div>
              </h3>
              <form action="" method="get" class="ui large form">
                <div class="ui stacked secondary  segment">
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                      <input type="text" name="email" placeholder="email address"/>
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="lock icon"></i>
                      <input type="password" name="password" placeholder="Password"/>
                    </div>
                  </div>
                  <div class="ui fluid large blue submit button">Login</div>
                </div>

                <div class="ui error message"></div>
              </form>

              <div class="ui message">
                Forgot password? <a href="">Create new password</a>
              </div>
            </div>
        </div>
    )
}

export default Login;