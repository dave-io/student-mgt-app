import React, { Component } from 'react';
import axios from 'axios';
import SearchBar from './SearchBar';




const BaseUrl = 'http://127.0.0.1:8000/api'

class DeptTable extends Component {
    async onSearchSubmit(term) {
        const response = await axios.get(`${BaseUrl}/departments`, {
            params: {search: term},
            headers: {
            'Authorization': 'Token 01907b16497470defa1f579deefb31fa9032b24c'
        }
        });
    }

    state = {
        deptListing: [],
        error:null,
        isLoading:true,
        searchDeptListing:"Computer",
        filterData:{},
        deptHeader:{
            name: 'Department',
            number_of_students: 'Number of Students',
            floor_section: 'Floor Section',
            HOD: 'HOD',
            student_average_age: 'Students\' Average Age'
        },
        isHovering: false
      }
    
    componentDidMount() {
    //fetch data
    axios.get("http://127.0.0.1:8000/api/departments", {
        headers: {
        'Authorization': 'Token 01907b16497470defa1f579deefb31fa9032b24c'
        } 
    })
    .then(res =>{ 
        console.log(res.data)
        this.setState({
        deptListing: res,
        isLoading:false
    })})
    .catch(error => console.log(error));
    }

    updateDepartment = e => {
        this.setState({ searchDeptListing: e.target.value.substr(0, 20) });
      };
    // addDepartment = ()

    handleDelete = (id) => {
        let url =`${BaseUrl}/department/${id}`
        fetch(url, {method: 'DELETE'})
        .then(() => {
            console.log('removed');
         })
         .catch(err => {
           console.error(err)
       })
       this.DeleteId(id)
       }

    DeleteId = id => {
        const DeleteID = this.state.deptListing.filter(dept => dept._id !== id)
        this.setState({
        deptListing:DeleteID
        })
    }


    renderTableHeader() {
        let header = Object.values(this.state.deptHeader)
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    // renderTableData(data) {
     
    //     // let i = 1;
    //     return data.
    //  }


    handleMouseHover =() => {
        this.setState({isHovering:!this.state.isHovering});
    }

    gotoHome=()=>{
        const {history} = this.props
        localStorage.clear()
        history.push("/")
     }


    render() {
        const {isLoading} = this.state;
        //  let filteredDeptType =this.state.deptListing && this.state.deptListing.data.filter(dept=>{
        //      return  dept.indexOf(this.state.filteredDeptType.toLowerCase()) > -1
        //   });

        return(
            <React.Fragment>
                <h3 id='title'>Currently Approved List of Departments</h3>
              
                
                <input type = "text" value={this.state.searchDept} onChange={this.updateDepartment} placeholder = "search here"/>
                <div className='tableBorder'>
                    { !isLoading ?(
                <table id='deptListing' className="ui compact celled striped definition table" >
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.state.deptListing.data.reverse().map(dept => {
           const {_id, name, number_of_students, floor_section, HOD, student_average_age} = dept
           return (
              <tr key={_id}>
                <td>{name}</td>
                <td>{number_of_students}</td>
                <td>{floor_section}</td>
                <td>{HOD}</td>
                <td>{student_average_age}</td>
                 {/* <td><button onClick={()=>this.handleDelete(_id)}>Delete</button></td>              */}
              </tr>
           )
        })}
                    </tbody>
                    <tfoot class="full-width">
                        <tr>
                        <th></th>
                        <th colSpan="4">
                            <div class="ui right floated small primary icon button">Add Department</div>
                            <div class="ui small button">Edit</div>
                            <div class="ui small  disabled button" >Delete</div>
                        </th>
                        </tr>
                    </tfoot>
                </table>):"loading"}
                </div>
            </React.Fragment>
        )
    }
}

export default DeptTable;