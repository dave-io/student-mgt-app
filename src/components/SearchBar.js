import React from 'react';


export default class SearchBar extends React.Component {

    state = { term: '' };

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state.term);
    };


    render() {
        return (
            <React.Fragment>
            <div className="ui search" >
                <div className="ui icon input" onSubmit={this.onFormSubmit}>
                    <input className="prompt" type="text" placeholder="Search..." value={this.state.term} onChange={(e) => this.setState({ term: e.target.value })} />
                    <i className="search icon"></i>
                </div>
                <div class="results"></div>
            </div>


            <div class="ui action input">
                <form onSubmit={this.onFormSubmit} className="ui form">
                <input type="text" placeholder="Search..." value={this.state.term} onChange={(e) => this.setState({ term: e.target.value })}/>              
                {/* <div className="ui button">Search</div> */}
                </form>
            </div>
            </React.Fragment>
        )
    }
}

/*
 <div>
                    {/* <SearchBar onSubmit={this.onSearchSubmit} /> */
                   // {/* Found: {this.state.deptListing.length} result(s) */}
                  //  </div>
                    /* <div className='adminSidebar'>
                        <a className='p-4 pt-5 text-white' href={'/'}><i className="fas fa-home"></i></a>
                        <a className='p-4 pt-5 text-white' href={'/Add_Jobs'}><i className="fas fa-paste"></i></a>
                        <a className='p-4 pt-2 text-white' onClick ={this.gotoHome}><i className="fas fa-power-off"></i></a>
                        <a className='p-4 pb-5 text-white' href='/'><i className="fas fa-key"></i></a>
                 //   </div> */
