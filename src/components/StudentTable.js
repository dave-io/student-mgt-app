import React, { Component } from 'react';
import SearchBar from './SearchBar';



const BaseUrl = 'http://127.0.0.1:8000/api'

class StudentTable extends Component {
    state = {
        studentList: [],
        tableHeader:{
            first_name: 'First name',
            last_name: 'Last name',
            gender: 'Gender',
            date_of_birth: 'Date of Birth',
            registration_date: 'Registration Date',
            department: 'Department'
        },
        isHovering: false
      }

    
    componentDidMount() {
    //fetch data  
    fetch(`${BaseUrl}/students`, {
        method: 'GET',
        headers: {
        'Authorization': 'Token 01907b16497470defa1f579deefb31fa9032b24c'
        } 
    }).then(response => response.json())
    .then(resp => this.setState({studentList: resp}))
    .catch(error => console.log(error));
    }

    handleDelete = (id) => {
        let url = `${BaseUrl}/students/${id}`
        fetch(url, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': 'Token 01907b16497470defa1f579deefb31fa9032b24c'
            }
        })
        .then(resp => {console.log(resp)
        })
         .catch(err => {
           console.error(err)
       })
       this.DeleteId(id)
       }

    DeleteId = id => {
        const DeleteID = this.state.studentList.filter(student => student._id !== id)
        this.setState({ studentList: DeleteID })
    }

    // deleteStudent = async (id) => {
    //     try {
    //         const res = await fetch(`${BaseUrl}/students/${id}`, {
    //             method: 'DELETE',
    //             mode: 'cors'
    //         });
    //         return res;
    //     }
    //     catch (err) {
    //         return err;
    //     }
        
    // }

    // deleteHandler(i, e) {
    //     e.preventDefault();
    //     this.props.onDelete(this.props.studentList[i].id);
    // };

    // onDelete(id) {
    //     deleteStudent(id)
    //     .then((data) => {
    //         let studentList = this.state.studentList.filter((student) => {
    //             return _id !== student.id;
    //         });
    //         this.setState(state => {
    //             state.studentList = studentList;
    //             return state;
    //         });
    //     })
    //     .catch((err) => {
    //         console.error('err', err);
    //     });
    // }

    

    renderTableHeader() {
        let header = Object.values(this.state.tableHeader)
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
     
        let i = 1;
        return this.state.studentList.reverse().map(student => {
           const {_id, first_name, last_name, gender, date_of_birth, registration_date, department} = student
           return (
              <tr key={_id}>
                <td>{first_name}</td>
                <td>{last_name}</td>
                <td>{gender}</td>
                <td>{date_of_birth}</td>
                <td>{registration_date}</td>
                <td>{department.name}</td>
                <td><button className="ui secondary button">Edit</button></td>
                <td><button onClick={() => this.deleteHandler(i)} className="ui red button">Delete</button></td>             
              </tr>
           )
        })
     }


    handleMouseHover =() => {
        this.setState({isHovering: !this.state.isHovering});
    }

    gotoHome=()=>{
        const {history} = this.props
        localStorage.clear()
        history.push("/")
     }
     

    render() {
        return(
            <React.Fragment>
                <h3 id='title'>List of Students</h3>
                <SearchBar />
                {/* <div className='adminSidebar'>
                    <a className='p-4 pt-5 text-white' href={'/'}><i className="fas fa-home"></i></a>
                    <a className='p-4 pt-5 text-white' href={'/Add_Jobs'}><i className="fas fa-paste"></i></a>
                    <a className='p-4 pt-2 text-white' onClick ={this.gotoHome}><i className="fas fa-power-off"></i></a>
                    <a className='p-4 pb-5 text-white' href='/'><i className="fas fa-key"></i></a>
                </div> */}
                <div className='tableBorder'>
                <table id='studentList' className="ui compact celled striped definition table" >
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                    <tfoot class="full-width">
                        <tr>
                        <th></th>
                        <th colSpan="4">
                            <div class="ui right floated small primary icon button">Add Student</div>
                            <div class="ui small button">Edit</div>
                            <div class="ui small  disabled button">Delete</div>
                        </th>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </React.Fragment>
        )
    }
}

export default StudentTable;