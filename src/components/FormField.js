import React, { Component } from 'react'
import { Divider, Form, Label } from 'semantic-ui-react'
import Axios from 'axios';

class Student extends Component {
    state = {
            firstName:"",
            lastName:"",
            gender:"",
            dob:"",
            registrationDate:"",
            contactAddress:"",
            department:""
        };
      
      handleChange = (e)=>{
            const value = e.target.value;
            this.setState({
                ...this.state,
                [e.target.name]:value
            });
      }

      handleSubmit=(e)=>{
          e.preventDefault();
          const {firstName, lastName, gender, dob, registrationDate, contactAddress, department} = this.state
          const model = {
            firstName, lastName, gender, dob, registrationDate, contactAddress, department
          }
          Axios.post("http://127.0.0.1:8000/api/students",{
            data:model
        })

      }
      
      
     
    render() {
        const {firstName, lastName, gender, dob, registrationDate, contactAddress, department} = this.state
      return (

  <Form className="ui" onSubmit = {this.handleSubmit}>
    <Form.Field>
      <input type='text' placeholder='First name' value={firstName} name="firstName" onChange={this.handleChange}/>
      <Label pointing>Please enter a value</Label>
    </Form.Field>
    <Divider />

    <Form.Field>
      <Label pointing='below'>Please enter a value</Label>
      <input type='text' placeholder='Last name' value={lastName} name="lastName" onChange={this.handleChange} />
    </Form.Field>
    <Divider />

    <Form.Field inline>
      <input type='text' placeholder='Gender' value={gender} name="gender" onChange={this.handleChange}/>
      <Label pointing='left'>Please enter a value</Label>
    </Form.Field>
    <Divider />

    <Form.Field inline>
      <Label pointing='right'></Label>
      <input type='text' placeholder='Date of birth' value={dob} name="dob" onChange={this.handleChange}/>
    </Form.Field>
    <Divider />

    <Form.Field inline>
      <Label pointing='right'></Label>
      <input type='text' placeholder='Registration Date' value={registrationDate} name="registrationDate" onChange={this.handleChange}/>
    </Form.Field>
    <Divider />

    <Form.Field inline>
      <Label pointing='right'></Label>
      <input type='text' placeholder='Contact Address' value={contactAddress} name="contactAddress" onChange={this.handleChange}/>
    </Form.Field>
    <Divider />

    <Form.Field inline>
      <Label pointing='right'></Label>
      <input type='text' placeholder='Department' value={department} name="department" onChange={this.handleChange}/>
    </Form.Field>
    <input type = "submit"/>
  </Form>
);
      }
    }

export default Student;
